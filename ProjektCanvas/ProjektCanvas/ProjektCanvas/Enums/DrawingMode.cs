﻿namespace ProjektCanvas.Enums;
internal enum DrawingMode
{
    DRAW = 0,
    LINE,
    EDIT_LINE,
    CIRCLE,
    RECTANGLE,
    POLYGON,
    POLYLINE,
    CHRISTMAS_TREE,
    TRAPEZOID,
    PLUS_SIGN,
    ERASER
}
