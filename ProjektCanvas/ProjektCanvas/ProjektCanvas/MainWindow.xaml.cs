﻿using Emgu.CV.Structure;
using Emgu.CV;
using Microsoft.Win32;
using ProjektCanvas.Enums;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using ProjektCanvas.Windows;
namespace ProjektCanvas
{
    public partial class MainWindow : Window
    {
        private System.Windows.Point currentPoint = new System.Windows.Point();
        private System.Windows.Media.Color drawingColor = System.Windows.Media.Color.FromRgb(0, 0, 0);
        private Line tempLine;
        private Line? selectedLine;
        private Shape? tempShape;
        private Polygon? tempPolygon;
        private Polyline? tempPolyline;
        private System.Windows.Shapes.Ellipse? tempEllipse;
        private bool isDragging = false;
        private bool isDraggingStart = false;
        private int eraserThickness = 5;
        private DrawingMode Mode = DrawingMode.DRAW;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Canvas_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            switch (Mode)
            {
                case DrawingMode.DRAW:
                    if (e.ButtonState == MouseButtonState.Pressed)
                        currentPoint = e.GetPosition(this);
                    break;
                case DrawingMode.LINE:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);

                        tempLine = new Line
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            X1 = currentPoint.X,
                            Y1 = currentPoint.Y,
                            X2 = currentPoint.X,
                            Y2 = currentPoint.Y
                        };

                        paintSurface.Children.Add(tempLine);
                    }
                    break;
                case DrawingMode.EDIT_LINE:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        var mousePos = e.GetPosition(paintSurface);
                        selectedLine = null;

                        foreach (var child in paintSurface.Children)
                        {
                            if (child is Line line)
                            {
                                if (Math.Abs(line.X1 - mousePos.X) < 15 && Math.Abs(line.Y1 - mousePos.Y) < 15)
                                {
                                    selectedLine = line;
                                    isDragging = true;
                                    isDraggingStart = true;
                                    break;
                                }
                                else if (Math.Abs(line.X2 - mousePos.X) < 15 && Math.Abs(line.Y2 - mousePos.Y) < 15)
                                {
                                    selectedLine = line;
                                    isDragging = true;
                                    isDraggingStart = false;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case DrawingMode.CIRCLE:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);
                        tempEllipse = new System.Windows.Shapes.Ellipse
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            StrokeThickness = 2 
                        };
                        Canvas.SetLeft(tempEllipse, currentPoint.X);
                        Canvas.SetTop(tempEllipse, currentPoint.Y);

                        paintSurface.Children.Add(tempEllipse);
                    }
                    break;
                case DrawingMode.RECTANGLE:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);
                        var rectangle = new System.Windows.Shapes.Rectangle
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            StrokeThickness = 2
                        };
                        paintSurface.Children.Add(rectangle);
                        tempShape = rectangle;
                    }
                    break;
                case DrawingMode.POLYGON:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        if (tempPolygon == null)
                        {
                            tempPolygon = new Polygon
                            {
                                Stroke = new SolidColorBrush(drawingColor),
                                StrokeThickness = 2
                            };
                            paintSurface.Children.Add(tempPolygon);
                        }

                        var point = e.GetPosition(paintSurface);
                        tempPolygon.Points.Add(point);
                    }
                    break;
                case DrawingMode.POLYLINE:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        if (tempPolyline == null)
                        {
                            tempPolyline = new Polyline
                            {
                                Stroke = new SolidColorBrush(drawingColor),
                                StrokeThickness = 2
                            };
                            paintSurface.Children.Add(tempPolyline);
                        }
                        var point = e.GetPosition(paintSurface);
                        tempPolyline.Points.Add(point);
                    }
                    break;
                case DrawingMode.CHRISTMAS_TREE:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);

                        for (int i = 0; i < 3; i++)
                        {
                            var triangle = new Polygon
                            {
                                Stroke = new SolidColorBrush(drawingColor),
                                Fill = new SolidColorBrush(drawingColor),
                                StrokeThickness = 2,
                                Points = new System.Windows.Media.PointCollection
                                {
                                    new System.Windows.Point(currentPoint.X, currentPoint.Y - 20 * i),
                                    new System.Windows.Point(currentPoint.X - 30, currentPoint.Y + 20 * (i + 1)),
                                    new System.Windows.Point(currentPoint.X + 30, currentPoint.Y + 20 * (i + 1))
                                }
                            };
                            paintSurface.Children.Add(triangle);
                        }

                        var trunk = new System.Windows.Shapes.Rectangle
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            Fill = new SolidColorBrush(drawingColor),
                            Width = 20,
                            Height = 30,
                            StrokeThickness = 2
                        };

                        Canvas.SetLeft(trunk, currentPoint.X - 10);
                        Canvas.SetTop(trunk, currentPoint.Y + 60);
                        paintSurface.Children.Add(trunk);
                    }
                    break;
                case DrawingMode.TRAPEZOID:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);

                        var trapezoid = new Polygon
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            Fill = new SolidColorBrush(drawingColor),
                            StrokeThickness = 2,
                            Points = new System.Windows.Media.PointCollection
                            {
                                new System.Windows.Point(currentPoint.X - 40, currentPoint.Y),
                                new System.Windows.Point(currentPoint.X + 40, currentPoint.Y),
                                new System.Windows.Point(currentPoint.X + 20, currentPoint.Y + 40),
                                new System.Windows.Point(currentPoint.X - 20, currentPoint.Y + 40)
                            }
                        };
                        paintSurface.Children.Add(trapezoid);
                    }
                    break;
                case DrawingMode.PLUS_SIGN:
                    if (e.ButtonState == MouseButtonState.Pressed)
                    {
                        currentPoint = e.GetPosition(paintSurface);

                        var verticalRect = new System.Windows.Shapes.Rectangle
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            Fill = new SolidColorBrush(drawingColor),
                            Width = 10,
                            Height = 40,
                            StrokeThickness = 2
                        };
                        Canvas.SetLeft(verticalRect, currentPoint.X - 5);
                        Canvas.SetTop(verticalRect, currentPoint.Y - 20);
                        paintSurface.Children.Add(verticalRect);

                        var horizontalRect = new System.Windows.Shapes.Rectangle
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            Fill = new SolidColorBrush(drawingColor),
                            Width = 40,
                            Height = 10,
                            StrokeThickness = 2
                        };
                        Canvas.SetLeft(horizontalRect, currentPoint.X - 20);
                        Canvas.SetTop(horizontalRect, currentPoint.Y - 5);
                        paintSurface.Children.Add(horizontalRect);
                    }
                    break;
                case DrawingMode.ERASER:
                    if (e.ButtonState == MouseButtonState.Pressed)
                        currentPoint = e.GetPosition(this);
                    break;
                default:
                    break;
            }
        }
        private void Canvas_MouseMove_1(object sender, MouseEventArgs e)
        {
            switch (Mode)
            {
                case DrawingMode.DRAW:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        Line line = new Line();
                        line.Stroke = new SolidColorBrush(drawingColor);
                        line.X1 = currentPoint.X;
                        line.Y1 = currentPoint.Y;
                        line.X2 = e.GetPosition(this).X;
                        line.Y2 = e.GetPosition(this).Y;
                        currentPoint = e.GetPosition(this);
                        paintSurface.Children.Add(line);
                    }
                    break;
                case DrawingMode.LINE:
                    if (e.LeftButton == MouseButtonState.Pressed && tempLine != null)
                    {
                        var position = e.GetPosition(paintSurface);

                        tempLine.X2 = position.X;
                        tempLine.Y2 = position.Y;
                    }
                    break;
                case DrawingMode.EDIT_LINE:
                    if (isDragging && selectedLine != null)
                    {
                        var position = e.GetPosition(paintSurface);

                        if (isDraggingStart)
                        {
                            selectedLine.X1 = position.X;
                            selectedLine.Y1 = position.Y;
                        }
                        else
                        {
                            selectedLine.X2 = position.X;
                            selectedLine.Y2 = position.Y;
                        }
                    }
                    break;
                case DrawingMode.CIRCLE:
                    if (tempEllipse != null && e.LeftButton == MouseButtonState.Pressed)
                    {
                        var position = e.GetPosition(paintSurface);
                        var diffX = position.X - currentPoint.X;
                        var diffY = position.Y - currentPoint.Y;

                        tempEllipse.Width = Math.Abs(diffX * 2);
                        tempEllipse.Height = Math.Abs(diffY * 2);
                        Canvas.SetLeft(tempEllipse, currentPoint.X - diffX);
                        Canvas.SetTop(tempEllipse, currentPoint.Y - diffY);
                    }
                    break;
                case DrawingMode.RECTANGLE:
                    if (tempShape is System.Windows.Shapes.Rectangle rectangle && e.LeftButton == MouseButtonState.Pressed)
                    {
                        var position = e.GetPosition(paintSurface);
                        var diffX = position.X - currentPoint.X;
                        var diffY = position.Y - currentPoint.Y;
                        rectangle.Width = Math.Abs(diffX);
                        rectangle.Height = Math.Abs(diffY);
                        Canvas.SetLeft(rectangle, Math.Min(currentPoint.X, position.X));
                        Canvas.SetTop(rectangle, Math.Min(currentPoint.Y, position.Y));
                    }
                    break;
                case DrawingMode.ERASER:
                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        Line line = new Line();
                        line.Stroke = paintSurface.Background;
                        line.StrokeThickness = eraserThickness; 
                        line.X1 = currentPoint.X;
                        line.Y1 = currentPoint.Y;
                        line.X2 = e.GetPosition(paintSurface).X;
                        line.Y2 = e.GetPosition(paintSurface).Y;
                        currentPoint = e.GetPosition(paintSurface);
                        paintSurface.Children.Add(line);
                    }
                    break;
                default:
                    break;
            }
        }
        private void Canvas_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            switch (Mode)
            {
                case DrawingMode.DRAW:

                    break;
                case DrawingMode.LINE:
                    if (tempLine != null)
                    {
                        var position = e.GetPosition(paintSurface);

                        Line finalLine = new Line
                        {
                            Stroke = new SolidColorBrush(drawingColor),
                            X1 = currentPoint.X,
                            Y1 = currentPoint.Y,
                            X2 = position.X,
                            Y2 = position.Y
                        };

                        paintSurface.Children.Add(finalLine);

                        paintSurface.Children.Remove(tempLine);
                        tempLine = null;
                    }
                    break;
                case DrawingMode.EDIT_LINE:
                    if (isDragging)
                    {
                        isDragging = false;
                        selectedLine = null; // Opcjonalnie resetuj wybraną linię
                    }
                    break;
                case DrawingMode.CIRCLE:
                    tempEllipse = null;
                    break;
                case DrawingMode.RECTANGLE:
                    tempShape = null;
                    break;
                default:
                    break;
            }
        }
        private void DrawButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.DRAW;
        }
        private void LineButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.LINE;
        }
        private void EditLineButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.EDIT_LINE;
            selectedLine = null;
            isDragging = false;
            isDraggingStart = false;
        }
        private void CircleButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.CIRCLE;
        }
        private void RectangleButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.RECTANGLE;
        }
        private void PolygonButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.POLYGON;
        }
        private void StartPolylineButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.POLYLINE;
            tempPolyline = null;
        }
        private void EndPolylineButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.DRAW;
            tempPolyline = null;
        }
        private void TreeButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.CHRISTMAS_TREE;
        }
        private void TrapezoidButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.TRAPEZOID;
        }
        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.PLUS_SIGN;
        }
        private void EraserButton_Click(object sender, RoutedEventArgs e)
        {
            Mode = DrawingMode.ERASER;
        }
        private void AddPhotoButton_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Pliki obrazów (*.jpg;*.png)|*.jpg;*.png"
            };
            if (openFileDialog.ShowDialog() == true)
            {
                var image = new BitmapImage(new Uri(openFileDialog.FileName));
                var imageControl = new System.Windows.Controls.Image
                {
                    Source = image
                };
                paintSurface.Children.Add(imageControl);
            }
        }
        private void ApplySobelFilter_Click(object sender, RoutedEventArgs e)
        {
            if (paintSurface.Children.OfType<System.Windows.Controls.Image>().Any())
            {
                var wpfImage = paintSurface.Children.OfType<System.Windows.Controls.Image>().First();
                BitmapSource bitmapSource = (BitmapSource)wpfImage.Source;
                using (MemoryStream stream = new MemoryStream())
                {
                    BitmapEncoder encoder = new BmpBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    encoder.Save(stream);
                    Bitmap bitmap = new Bitmap(stream);
                    int width = bitmap.Width;
                    int height = bitmap.Height;
                    byte[,,] data = new byte[height, width, 3];
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            System.Drawing.Color pixelColor = bitmap.GetPixel(x, y);
                            data[y, x, 0] = pixelColor.B;
                            data[y, x, 1] = pixelColor.G;
                            data[y, x, 2] = pixelColor.R;
                        }
                    }
                    Image<Bgr, byte> emguImage = new Image<Bgr, byte>(data);
                    Image<Gray, byte> emguImage2 = emguImage.Convert<Gray, byte>();
                    Image<Gray, Single> final = emguImage2.Sobel(1, 0, 5);
                    BitmapSource resultBitmapSource = ToBitmapSource(final, width, height);
                    wpfImage.Source = resultBitmapSource;
                }
            }
        }
        private BitmapSource ToBitmapSource(Image<Gray, Single> image, double imageWidth, double imageHeight)
        {
            byte[] data = new byte[image.Width * image.Height];
            int index = 0;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    data[index] = (byte)Math.Round(image[y, x].Intensity * 255);
                    index++;
                }
            }
            BitmapSource bitmapSource = BitmapSource.Create(
                Convert.ToInt32(imageWidth), Convert.ToInt32(imageHeight), 250, 250, PixelFormats.Gray8, null, data, image.Width);
            return bitmapSource;
        }
        private void FilterImage_Click(object sender, RoutedEventArgs e)
        {
            if (paintSurface.Children.OfType<System.Windows.Controls.Image>().Any())
            {
                var wpfImage = paintSurface.Children.OfType<System.Windows.Controls.Image>().First();
                BitmapSource bitmapSource = (BitmapSource)wpfImage.Source;
                var filteredImage = ApplyLowPassFilter(bitmapSource);
                wpfImage.Source = filteredImage;
            }
        }
        private BitmapSource ApplyLowPassFilter(BitmapSource source)
        {
            WriteableBitmap writeableBitmap = new WriteableBitmap(source);
            int width = source.PixelWidth;
            int height = source.PixelHeight;
            int stride = width * ((source.Format.BitsPerPixel + 7) / 8);
            byte[] pixels = new byte[height * stride];
            source.CopyPixels(pixels, stride, 0);
            byte[] newPixels = new byte[height * stride];
            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width - 1; x++)
                {
                    for (int channel = 0; channel < 3; channel++)
                    {
                        int index = y * stride + x * 4 + channel;
                        int sum = 0;
                        for (int dy = -1; dy <= 1; dy++)
                        {
                            for (int dx = -1; dx <= 1; dx++)
                            {
                                int neighborIndex = index + dy * stride + dx * 4;
                                sum += pixels[neighborIndex];
                            }
                        }
                        newPixels[index] = (byte)(sum / 9);
                    }
                }
            }
            writeableBitmap.WritePixels(new Int32Rect(0, 0, width, height), newPixels, stride, 0);
            return writeableBitmap;
        }
        private void ColorPreview_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RGBInputWindow rgbInputDialog = new RGBInputWindow();
            bool? dialogResult = rgbInputDialog.ShowDialog();
            if (dialogResult == true)
            {
                int r = rgbInputDialog.R;
                int g = rgbInputDialog.G;
                int b = rgbInputDialog.B;
                drawingColor = System.Windows.Media.Color.FromRgb((byte)r, (byte)g, (byte)b);
                ColorPreview.Fill = new SolidColorBrush(drawingColor);
            }
        }
        private void EraserThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            eraserThickness = (int)e.NewValue;
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = "Pliki PNG (*.png)|*.png|Pliki JPG (*.jpg)|*.jpg",
                DefaultExt = "png"
            };
            if (saveFileDialog.ShowDialog() == true)
            {
                RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
                    (int)paintSurface.ActualWidth, (int)paintSurface.ActualHeight,
                    96d, 96d, PixelFormats.Pbgra32);
                renderBitmap.Render(paintSurface);
                using (FileStream file = File.Create(saveFileDialog.FileName))
                {
                    BitmapEncoder encoder;
                    if (System.IO.Path.GetExtension(saveFileDialog.FileName).ToLower() == ".jpg")
                    {
                        encoder = new JpegBitmapEncoder();
                    }
                    else
                    {
                        encoder = new PngBitmapEncoder();
                    }
                    encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                    encoder.Save(file);
                }
            }
        }
    }
}