﻿using System.Windows;
using System.Windows.Controls;
namespace ProjektCanvas.Windows
{
    public partial class RGBInputWindow : Window
    {
        public int R { get; private set; }
        public int G { get; private set; }
        public int B { get; private set; }
        public RGBInputWindow()
        {
            InitializeComponent();
        }
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(txtR.Text, out int r) &&
                int.TryParse(txtG.Text, out int g) &&
                int.TryParse(txtB.Text, out int b) &&
                (r >= 0 && r <=255) && (g >= 0 && g <= 255) && (b >= 0 && b <= 255))
            {
                DialogResult = true;
                R = r;
                G = g;
                B = b;
            }
            else
            {
                MessageBox.Show("Wprowadź poprawne wartości R, G i B. (wartości musza być od 0 do 255)", "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RGB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(txtR.Text, out int r) &&
                int.TryParse(txtG.Text, out int g) &&
                int.TryParse(txtB.Text, out int b) &&
                (r >= 0 && r <= 255) && (g >= 0 && g <= 255) && (b >= 0 && b <= 255))
            {
                txtHSV.Text = ConvertRGBtoHSV(r, g, b);
            }
        }
        private string ConvertRGBtoHSV(int r, int g, int b)
        {
            double delta, min;
            double h = 0, s, v;
            double rPrime = r / 255.0;
            double gPrime = g / 255.0;
            double bPrime = b / 255.0;
            min = Math.Min(Math.Min(rPrime, gPrime), bPrime);
            v = Math.Max(Math.Max(rPrime, gPrime), bPrime);
            delta = v - min;
            if (v == 0.0)
            {
                s = 0;
            }
            else
            {
                s = delta / v;
            }
            if (s == 0)
            {
                h = 0.0;
            }
            else
            {
                if (rPrime == v)
                {
                    h = (gPrime - bPrime) / delta;
                }
                else if (gPrime == v)
                {
                    h = 2 + (bPrime - rPrime) / delta;
                }
                else if (bPrime == v)
                {
                    h = 4 + (rPrime - gPrime) / delta;
                }
                h *= 60;
                if (h < 0.0)
                {
                    h += 360;
                }
            }       
            s *= 100;
            v *= 100;
            return $"{h:0.##}, {s:0.##}%, {v:0.##}%";
        }
    }
}
